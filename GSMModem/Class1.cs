﻿namespace GSMModem
{
    public interface IGSMModem
    {
        void Init(int portNum);
        string ReadData();
        void WriteData(string data);
        void DeInit();
    }
}
