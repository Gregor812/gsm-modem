﻿namespace GSMModem.Interfaces
{
    public interface IGSMModem
    {
        void Init();
        bool IsInitialized();
        byte[] ReadData();
        void SendData(byte[] data);
        void DeInit();
    }
}
