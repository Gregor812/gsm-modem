﻿using System;
using GSMModem.Interfaces;
using System.IO;
using System.IO.Ports;
using System.Text;

namespace GSMModem
{
    public class AirLink6110GL : IGSMModem
    {
        public AirLink6110GL(SerialPort port)
        {
            _port = port;
        }

        public void Init()
        {
            if (null == _port)
            {
                _isInitialized = false;
                return;
            }
            else
            {
                try
                {
                    _port.Open();
                }
                catch (IOException ex)
                {
                    Console.WriteLine("Cannot open serial _port {0}", _port.PortName);
                    Console.WriteLine(ex.Message);
                    Console.WriteLine(ex.ToString());
                    return;
                }
                Encoding ascii = Encoding.ASCII;
                string cmd = "AT+CGMM\r";
                byte[] asciiCmd = ascii.GetBytes(cmd);

                SendData(asciiCmd);

                byte[] received = null;
                received = ReadData();
                string unicodeReceivedData = ascii.GetString(received).Trim();
                if (unicodeReceivedData.Contains("GL6110"))
                    _isInitialized = true;

                Console.WriteLine(unicodeReceivedData);
            }
        }

        public bool IsInitialized()
        {
            return _isInitialized;
        }

        public byte[] ReadData()
        {
            int bufferSize = _port.ReadBufferSize;
            byte[] data = new byte[bufferSize];
            _port.Read(data, 0, _port.ReadBufferSize);
            return data;
        }

        public void SendData(byte[] data)
        {
            _port.Write(data, 0, data.Length);
        }

        public void DeInit()
        {
            
        }

        private SerialPort _port;
        private bool _isInitialized;
    }
}
