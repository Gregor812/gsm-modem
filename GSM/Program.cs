﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GSMModem;
using GSMModem.Interfaces;
using System.IO.Ports;

namespace GSM
{
    class Program
    {
        static void Main(string[] args)
        {
            SerialPort GSMModemPort = new SerialPort("COM3", 115200, Parity.None, 8, StopBits.One);
            GSMModemPort.WriteTimeout = 100;
            GSMModemPort.ReadTimeout = 100;
            IGSMModem modem = new AirLink6110GL(GSMModemPort);
            modem.Init();
            Console.WriteLine(modem.IsInitialized());
            Console.ReadKey();
        }
    }
}
